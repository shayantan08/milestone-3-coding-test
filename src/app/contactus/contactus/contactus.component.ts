import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { ServicesService } from 'src/app/services.service';


@Component({
  selector: 'app-query',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.scss']
})
export class ContactusComponent implements OnInit {
  constructor(private router:Router,private formbuilder:FormBuilder, private api:ServicesService) { }
  url="http://localhost:3000/queries";
  ngOnInit(): void {
  }
  
  formSubmited=false;
  myForm: FormGroup = this.formbuilder.group({
    name: ["", [Validators.required]],
    email: ["", [Validators.required]],
    mobile:["", [Validators.required]],
    subject:["", [Validators.required]],
    message: ["", [Validators.required]],
    
  })


  send()
  {
    if(this.myForm.status =="VALID")
    {
      let time = Date.now()
      let body=
      {
        id:time,
        name:this.myForm.value.name,
        email:this.myForm.value.email,
        mobile:this.myForm.value.mobile,
        subject:this.myForm.value.subject,
        message:this.myForm.value.message
      }

      this.api.post(this.url,body).subscribe(res=>{
        console.log(res)
        alert('Query Created');
         this.router.navigateByUrl('/view-query');
      })

    }else{
      alert('All Fields are Required');
    }
    
    
  }
    }