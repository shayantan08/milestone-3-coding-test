import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlacebookingRoutingModule } from './placebooking-routing.module';
import { PlacebookingComponent } from './placebooking/placebooking.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    PlacebookingComponent
  ],
  imports: [
    CommonModule,
    PlacebookingRoutingModule,
    FormsModule,    //import here
    ReactiveFormsModule //import here
  ]
})
export class PlacebookingModule { }
