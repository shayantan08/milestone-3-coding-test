import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { PlacebookingComponent } from './placebooking.component';

describe('PlacebookingComponent', () => {
  let component: PlacebookingComponent;
  let fixture: ComponentFixture<PlacebookingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlacebookingComponent ],
      providers: [

        HttpClient,

        FormBuilder

      ],

      imports: [

        RouterTestingModule,

        HttpClientModule

      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlacebookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
