import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactusModule } from './contactus/contactus.module';
import { HomeModule } from './home/home.module';
import { HttpclientInterceptor } from './httpclient.interceptor';
import { PlacebookingModule } from './placebooking/placebooking.module';
import { ViewbookingModule } from './viewbooking/viewbooking.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PlacebookingModule,
    ContactusModule,
    HomeModule,
    ViewbookingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    {

      provide: HTTP_INTERCEPTORS,
    
      useClass:HttpclientInterceptor,
    
      multi:true
    
      }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
