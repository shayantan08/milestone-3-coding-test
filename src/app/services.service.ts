import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
    
import {  Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
   
import { Services } from './services';
    
@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  private apiURL = "http://localhost:3000/emp";
    
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private httpClient: HttpClient) { }
  
  public getString (object: any) {
    let paramsAndValues = [];
    for(let key in object) {
      let value = encodeURIComponent(object[key].toString());
      paramsAndValues.push([key, value].join('='));
    }
    return paramsAndValues.join('&');
  }
  getAll(url: string, data?: any, options?: any) {
    return this.httpClient.get(url +"?"+ this.getString(data), options);
  }  
  getData(){
    return this.httpClient.get(this.apiURL)
  }
  public post(url: string, data?: any, options?: any) {
    // data = this.postString(data);
    return this.httpClient.post(url, data, options);
  }  
}
