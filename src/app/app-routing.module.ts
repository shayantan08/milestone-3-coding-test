import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactusComponent } from './contactus/contactus/contactus.component';
import { HomeComponent } from './home/home/home.component';
import { PlacebookingComponent } from './placebooking/placebooking/placebooking.component';
import { ViewbookingComponent } from './viewbooking/viewbooking/viewbooking.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component:HomeComponent},
  { path: 'placebooking', component:PlacebookingComponent},
  { path: 'viewbooking', component:ViewbookingComponent},
  { path: 'contactus', component:ContactusComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
