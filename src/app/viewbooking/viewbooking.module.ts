import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewbookingComponent } from './viewbooking/viewbooking.component';



@NgModule({
  declarations: [
    ViewbookingComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ViewbookingModule { }
