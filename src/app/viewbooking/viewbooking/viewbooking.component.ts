import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../services.service';
import { Services } from '../../services';

@Component({
  selector: 'app-viewbooking',
  templateUrl: './viewbooking.component.html',
  styleUrls: ['./viewbooking.component.scss']
})
export class ViewbookingComponent implements OnInit {

  posts: Services[] = [];
  url ="http://localhost:3000/emp";
  constructor(public api:ServicesService) { }
  appointments:any;
     
  /*------------------------------------------
  --------------------------------------------
  Created constructor
  --------------------------------------------
  --------------------------------------------*/
  // constructor(public servicesService: ServicesService) { }
  
  ngOnInit(): void {
    this.api.getAll(this.url).subscribe(res=>{
      this.appointments =res;
    })
  }

  trackByIndex = (index:number):number =>{
    return index;
  }
  }
     
  // ngOnInit(): void {
  //   this.servicesService.getData().subscribe(res=>{
  //     this.data = res ; 
  //   })
  // }




